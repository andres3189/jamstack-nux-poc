# poc-jamstack-stroy-book-npm-app

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## Creation Process

### # Nuxt installation

1. Package manager

   - NPM (Remommended)

2. Programming language

   - TypeScript

3. UI framework:

   - None (feel free to add one later)

4. Nuxt.js modules:

   - None (Chose one if you want)

5. Linting tools:

   - None (Chose one if you want)

6. Testing framework:

   - Jest (Recommended)

7. Rendering mode

   - Universal (SSR / Static) (Important)

8. Deployment target

   - Static (Static/JAMStack hosting)

9. Development tools

   - None (You can add one or more)

10. Continuous Integration
    - None (You can use your preferred)

You can read more in [create-nuxt-app documentation]('https://github.com/nuxt/create-nuxt-app')

### # Story Book Instalation

```bash
npm install --save-dev @nuxtjs/storybook postcss@latest
```

Add to git Ignore

```bash

.nuxt-storybook

storybook-static

```

More in the [nuxt/storybook documentation](https://storybook.nuxtjs.org/getting-started/installation)

More about [Story Book](https://storybook.js.org/docs/vue/get-started/whats-a-story)

### # Install Sass to the project

Execute the next code into the terminal

```bash
npm install --save-dev sass sass-loader@10  @nuxtjs/style-resources
```

Add the next folder indentation to the project

- Assets
  - scss
    - mixins.scss
    - variables.scss
  - css
    - main.css

Add in your nuxt configuration the files

_nuxt.config.js_

```bash
# add main.css file to the nuxt project
css: [
    '~assets/css/main.css'
  ],

# add nuxt/style-resources module in the project
buildModules: [
    '@nuxtjs/style-resources'
  ],

# add scss files into the project
styleResources: {
    scss: [
      '~assets/scss/mixins.scss',
      '~assets/scss/variables.scss'
    ]
  },
```

**!IMPORTANT: The styles share between vue files and vue componentes, take care.**

For more about add Sass in nuxt project [read here](https://dev.to/nbhankes/using-sass-in-nuxt-js-4595) .

[Other way]('https://stackoverflow.com/questions/68728903/how-to-setup-sass-scss-sass-loader-in-nuxt)

### # Deploy with docker

You can add docker static files compilation into the project.

_Dockerfile_ example

```bash
FROM node:16.13.1

# create destination directory
RUN mkdir -p /usr/src/nuxt-app
WORKDIR /usr/src/nuxt-app

# update and install dependency
RUN apt update && apt upgrade
RUN apt install git -y

# copy the app, note .dockerignore
COPY . /usr/src/nuxt-app/
RUN npm install

# build necessary, even if no static files are needed,
# since it builds the server as well
RUN npm run generate

# expose 5000 on container
EXPOSE 5000

# set app serving to permissive / assigned
ENV NUXT_HOST=0.0.0.0
# set app port
ENV NUXT_PORT=5000

# start the app
CMD [ "npm", "start" ]
```

You can edit the Dockerfile if as you need, but its important use the following comand line.

```bash
# with this you can generate a static file compilation.
RUN npm run generate
```

Comands lines:

```bash
# To run the builder
docker build -t my_app .

# To execute the container
docker run -it -p 5000:5000 my_app
```

more about [Deploying a nuxt project with docker](https://jonathanmh.com/deploying-a-nuxt-js-app-with-docker/)

### # About

- [Nuxt Documentaiton](https://nuxtjs.org/)

- [Create Nuxt app Documentation](https://github.com/nuxt/create-nuxt-app)

- [Nuxt/typescript Documentation](https://typescript.nuxtjs.org/es)

- [Nuxt/storybook Documentation](https://storybook.nuxtjs.org/getting-started/installation)

- [Nuxt app with Sass repo config](https://github.com/kissu/so-nuxt-working-sass-scss/blob/master/nuxt.config.js)

- [Vue storybook Documentaiton](https://storybook.js.org/docs/vue/get-started/whats-a-story)

### # Work Nuxt with Mark Down language

You can work with MD language or YML files with nuxt to empower your nuxt aplication.

You can read more in the following links.

- [nuxt/content documentation](https://v3.vuejs.org/)

- [Create a nuxt blog jamstack with nuxt/content](https://nuxtjs.org/tutorials/creating-blog-with-nuxt-content/)

- [Project with nuxt content and Tailwind css](https://github.com/nuxt/content/blob/master-old/packages/theme-docs/src/assets/css/main.css)

- [Explanation and guiw of nuxt/content with Mark Down language by **Maya Shavin**](https://mayashavin.com/articles/power-up-jamstack-nuxt)

- [Project nuxt with Mark Down language by **Maya Shavin**](https://github.com/mayashavin/tours-nuxt-full-static)
